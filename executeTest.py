import argparse, json, time, urllib ,urllib2, threading, sys
from exceptions import Exception
from threading import Thread

parser = argparse.ArgumentParser()
parser.add_argument("apiKey", type=str,help="Unique identifier of the company")
parser.add_argument("deviceList", type=str,help="list of devices for test")
parser.add_argument("testSuiteId", type=str,help="suite id for test")

threads = []
exit = 0

print 'sending execute...'
args = parser.parse_args()
url = 'http://blink.devicelab.com.br/api/executeTask' 
values = {'apiKey' : args.apiKey,
          'deviceList' : args.deviceList,
          'testSuiteId' : args.testSuiteId }

data = urllib.urlencode(values)
req = urllib2.Request(url, data)
response = urllib2.urlopen(req).read()

def checkStatus(task):
    print 'checking status for task : '+ task
    while True:
        url = 'http://blink.devicelab.com.br/api/tasks/result/'+args.apiKey+'/'+task[1:-1] 
        status = urllib2.urlopen(url).read()    
        n = json.dumps(status)  
        response = json.loads(n)
        if len(response) > 20 :    
            j = json.loads(response)
            string = 'Task : '+j['results'][0]['task']['refId']['name'] + ' has finished with '+j['results'][0]['status'] + ' on ' + j['results'][0]['device']['model']['name']
            print string.encode('utf-8')
            if j['results'][0]['status'] == 'FAIL':
                global exit
                exit = -1
            break
        time.sleep(5)
response = response[1:-1]
ntask = response.split(",");

for task in ntask:
    t = Thread(target=checkStatus, args=(task,))
    threads.append(t)
    t.start()

for x in threads:
    x.join()

sys.exit(exit)