import argparse, json, time, threading, requests, os, sys, urllib2


from threading import Thread
from requests_toolbelt.multipart.encoder import MultipartEncoder 

parser = argparse.ArgumentParser()
parser.add_argument("apiKey", type=str,help="Unique identifier of the company")
parser.add_argument("devices", type=str,help="list of devices for test")
parser.add_argument("appName", type=str,help="Application Name")
parser.add_argument("version", type=str,help="Application version")
parser.add_argument("platform", type=str,help="Application Platform")
parser.add_argument("file", type=str,help="Application file path")

reload(sys)
sys.setdefaultencoding('utf8')

exit = 0
def checkInstallation(hash):
    while True:
        url = 'http://blink.devicelab.com.br/api/checkInstallation/'+args.apiKey+'/'+hash[1:-1]
        response = requests.get(url, headers={'Content-Type': multipart_data.content_type})
        if response.text[1:-1] == 'COMPLETED':
            url = 'http://blink.devicelab.com.br/api/checkInstallation/result/'+args.apiKey+'/'+hash[1:-1]
            status = urllib2.urlopen(url).read()    
            n = json.dumps(status)  
            response = json.loads(n)
            array = json.loads(response)
            print array
            for i in array:
                string ="Installed with "+ str(i['status'])+' on '+ str(i['device']['model']['name'])
                print string
                if str(i['status']) == 'FAIL' :
                    exit = -1
            break
        time.sleep(5)

args = parser.parse_args()

multipart_data = MultipartEncoder(
    fields = {
    'apiKey': args.apiKey,
    'devices': args.devices,
    'name': args.appName,
    'version' : args.version,
    'platform': args.platform,
    'file' : (args.appName, open(args.file, 'rb'))
    })

url = 'http://blink.devicelab.com.br/api/installApp'
response = requests.post(url, data=multipart_data, headers={'Content-Type': multipart_data.content_type})
print response.text

checkInstallation(response.text)

sys.exit(exit)
